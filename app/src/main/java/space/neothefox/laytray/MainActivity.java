package space.neothefox.laytray;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Map;

public class MainActivity extends AppCompatActivity
implements View.OnClickListener, DialogInterface.OnClickListener, SharedPreferences.OnSharedPreferenceChangeListener {

    public final String TAG = "layiconActivity";

    private LinearLayout layoutLister;
    private SharedPreferences layouts;
    private SharedPreferences iconsPrefs;
	private boolean shouldRefresh = true;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        layoutLister = findViewById(R.id.scrollLinearLayout);
        layouts = getSharedPreferences("layouts", 0);
        iconsPrefs = getSharedPreferences("icons", 0);
        updateLayouts();

        FloatingActionButton addButton = findViewById(R.id.floatingActionButton);
        addButton.setOnClickListener(this);
        layouts.registerOnSharedPreferenceChangeListener(this);

        if (!isAccessibilitySettingsOn(getApplicationContext()))
        {
            Toast.makeText(this, R.string.toast_enableme,
                    Toast.LENGTH_LONG).show();
            startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
        }

        if(!isPackageInstalled("com.blackberry.keyboard", getApplicationContext().getPackageManager()) && !BuildConfig.DEBUG)
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.unsupported_device)
                    .setMessage(R.string.unsupported_device_description)
                    .setNeutralButton(R.string.close, this)
                    .show();
        }
    }

    private void updateLayouts()
    {
        layoutLister.removeAllViewsInLayout();

        Log.d(TAG, "Updating Layouts:");
        Map<String,?> keys = layouts.getAll();
        if(keys != null)
        {
            Log.d(TAG, "listing map:");
            int i = 0;
            for(Map.Entry<String,?> entry : keys.entrySet())
            {
                Log.d("map values",entry.getKey() + ": " +
                        entry.getValue().toString());
                i++;

                if(!entry.getKey().equals("EMPT")) {
                    addLine(
                            layoutLister, entry.getKey(),
                            entry.getValue().toString(),
                            iconsPrefs.getInt(entry.getKey(), R.drawable.ic_language_default)
                    );
                }
            }
            if(i == 0)
                populateLayouts();
        }
        else
            populateLayouts();

    }

    private void populateLayouts()
    {
        Log.d("map values", "Shared Prefs are empty");
        SharedPreferences.Editor layoutsEditor = layouts.edit();
        SharedPreferences.Editor iconsEditor = iconsPrefs.edit();
        iconsEditor.clear();
        layoutsEditor.clear();
        layoutsEditor.putString("Русский", "RU");
        layoutsEditor.putString("Буквы (АБВ)", "EN");
        layoutsEditor.putString("EMPT", "??");
		
		shouldRefresh = false;
        iconsEditor.apply();
		shouldRefresh = true;
        layoutsEditor.apply();
    }

    private void addLine(LinearLayout parent, String name, String icon, int iconDrawableId)
    {
        final LinearLayout layoutLine = new LinearLayout(getApplicationContext());
        layoutLine.setOrientation(LinearLayout.HORIZONTAL);
        layoutLine.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                                                 ViewGroup.LayoutParams.WRAP_CONTENT));

        TextView layoutName = new TextView(getApplicationContext());
        layoutName.setText(name);
        //workaround for Priv
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M) layoutName.setTextColor(Color.BLACK);
        layoutLine.addView(layoutName);

        EditText layoutIcon = new EditText(getApplicationContext());
        //workaround for Priv
        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M) layoutIcon.setTextColor(Color.BLACK);
        layoutIcon.setText(icon);
        layoutLine.addView(layoutIcon);


        // Icons Dropdown
        final int[] iconFlags = {
                R.drawable.ic_language_default,
                R.drawable.ic_flag_russia,
                R.drawable.ic_flag_gb,
                R.drawable.ic_flag_belarus,
                R.drawable.ic_flag_ukr
        };
        IconsAdapter arrayAdapter = new IconsAdapter(this, iconFlags);
        final Spinner iconSpinner = new Spinner(this);
        iconSpinner.setAdapter(arrayAdapter);
        iconSpinner.setSelection(arrayAdapter.getPosition(iconDrawableId));
        layoutLine.addView(iconSpinner);
        iconSpinner.setSelection(arrayAdapter.getPosition(iconDrawableId));


        Space space = new Space(getApplicationContext());
        space.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT, 2));
        layoutLine.addView(space);


        final Button removeButton = new Button(getApplicationContext());
        removeButton.setText("➖");
        removeButton.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                                                                   ViewGroup.LayoutParams.WRAP_CONTENT));
        removeButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                LinearLayout daddy = (LinearLayout)removeButton.getParent();
                LinearLayout grandpa = (LinearLayout)daddy.getParent();
                daddy.removeAllViewsInLayout();
                grandpa.removeView(daddy);
            }
        });
        layoutLine.addView(removeButton);

        parent.addView(layoutLine);
    }

    private void saveLayouts(LinearLayout parent)
    {
        int count = parent.getChildCount();
        Log.d(TAG, String.format("%d layouts to save", count));
        if (count != 0)
        {
            SharedPreferences.Editor layoutsEditor = layouts.edit();
            SharedPreferences.Editor iconsEditor = iconsPrefs.edit();
            layoutsEditor.clear();
            iconsEditor.clear();
            for (int i=0; i < count; i++)
            {
                LinearLayout layoutLine = (LinearLayout)parent.getChildAt(i);
                TextView layoutName = (TextView)layoutLine.getChildAt(0);
                EditText layoutIcon = (EditText)layoutLine.getChildAt(1);
                Spinner layoutDropdown = (Spinner)layoutLine.getChildAt(2);
                String layoutNameValue = layoutName.getText().toString();
                String layoutIconValue = layoutIcon.getText().toString();
                int layoutDropdownValue = (int)layoutDropdown.getSelectedItem();
                if(!layoutNameValue.equals(""))
                {
                    if(!layoutIconValue.equals("")) {
                        layoutsEditor.putString(layoutNameValue, layoutIconValue);
                        iconsEditor.putInt(layoutNameValue, layoutDropdownValue);
                    } else {
                        layoutsEditor.putString(layoutNameValue, "??");
                        iconsEditor.putInt(layoutNameValue, layoutDropdownValue);
                    }
                }
            }
			shouldRefresh = false;
            iconsEditor.apply();
			shouldRefresh = true;
            layoutsEditor.apply();
        } else {
            populateLayouts();
        }
    }


    //Accessibility check by Antoine Bolvy
    private boolean isAccessibilitySettingsOn(Context mContext)
    {
        int accessibilityEnabled = 0;
        final String service = getPackageName() + "/" + IconService.class.getCanonicalName();
        try
        {
            accessibilityEnabled = Settings.Secure.getInt(
                    mContext.getApplicationContext().getContentResolver(),
                    android.provider.Settings.Secure.ACCESSIBILITY_ENABLED);
            Log.v(TAG, "accessibilityEnabled = " + accessibilityEnabled);
        }
        catch (Settings.SettingNotFoundException e)
        {
            Log.e(TAG, "Error finding setting, default accessibility to not found: "
                    + e.getMessage());
        }
        TextUtils.SimpleStringSplitter mStringColonSplitter = new TextUtils.SimpleStringSplitter(':');

        if (accessibilityEnabled == 1)
        {
            Log.v(TAG, "Accessibility service enabled");
            String settingValue = Settings.Secure.getString(
                    mContext.getApplicationContext().getContentResolver(),
                    Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
            if (settingValue != null)
            {
                mStringColonSplitter.setString(settingValue);
                while (mStringColonSplitter.hasNext())
                {
                    String accessibilityService = mStringColonSplitter.next();

                    Log.v(TAG, "-------------- > accessibilityService :: " + accessibilityService + " " + service);
                    if (accessibilityService.equalsIgnoreCase(service))
                    {
                        Log.v(TAG, "We've found the correct setting - accessibility is switched on!");
                        return true;
                    }
                }
            }
        }
        else
            Log.v(TAG, "Accessibility is disabled");

        return false;
    }

    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.floatingActionButton:
                saveLayouts(layoutLister);
                Toast.makeText(this, R.string.toast_saved,
                        Toast.LENGTH_SHORT).show();
                break;

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId())
        {
            case R.id.settings:
                startActivity(new Intent(this, SettingsActivity.class));
                return true;
            case R.id.about:
                startActivity(new Intent(this, AboutActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
    {
		if (shouldRefresh) {
			layouts = getSharedPreferences("layouts", 0);
			iconsPrefs = getSharedPreferences("icons", 0);
			updateLayouts();
		}
    }

    private boolean isPackageInstalled(String packageName, PackageManager packageManager)
    {
    try
    {
        packageManager.getPackageInfo(packageName, 0);
        return true;
    }
    catch (PackageManager.NameNotFoundException e)
    {
        return false;
    }
}

    @Override
    public void onClick(DialogInterface dialogInterface, int i)
    {
        //There is only one potential dialog
        this.finish();
    }
}
