package space.neothefox.laytray;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

public class IconsAdapter extends BaseAdapter {
    private Context context;
    private int flags[];
    private LayoutInflater inflter;

    public IconsAdapter(Context applicationContext, int[] flags) {
        this.context = applicationContext;
        this.flags = flags;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return flags.length;
    }

    @Override
    public Object getItem(int i) {
        return flags[i];
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public int getPosition(int value) {
        int position = -1;
        for (int i = 0; i < flags.length; i++) {
            if (value == flags[i]) {
                position = i;
                break;
            }
        }
        return position;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        convertView = inflter.inflate(R.layout.spinner_item, null);
        ImageView icon = (ImageView) convertView.findViewById(R.id.imageView);
        icon.setImageResource(flags[i]);

        return convertView;
    }
}